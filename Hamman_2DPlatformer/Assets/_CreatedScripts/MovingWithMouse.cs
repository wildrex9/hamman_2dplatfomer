﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class MovingWithMouse : MonoBehaviour
{
    public Collider2D objectCollider;
    public GameObject level;
    public Collider2D anotherCollider;
    public Vector2 velocityCap;
    Vector2Int v2i;
    //patroller patrol;

    private Tilemap tilemap;

    PhysicsObject physO;
    private Vector3Int pPos;

    Vector2 motionTest;
    Vector2 prevPos;


    //public new Transform transform;
    //public bool MouseIsOver = false;
    // Start is called before the first frame update
    void Start()
    {
        //v2i = new Vector2Int((int)velocityCap.x, (int)velocityCap.y);
        physO = GetComponent<PhysicsObject>();
        objectCollider = GetComponent<BoxCollider2D>();
        anotherCollider = level.GetComponent<TilemapCollider2D>();
        velocityCap = new Vector2(0, -2);
        tilemap = level.GetComponent<Tilemap>();
        //patrol = GetComponent <patroller> ();

        prevPos = transform.position;
        
    }

    private void FixedUpdate()
    {
        Vector2 curPos = new Vector2(transform.position.x, transform.position.y);
        Vector2 delta = curPos - prevPos;
        motionTest = Vector2.MoveTowards(motionTest, delta, Time.deltaTime * 10f);
        prevPos = transform.position;

        //print("motion test: " + motionTest.x + " " + motionTest.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Vector3 hitPosition = Vector3.zero;
        //print(anotherCollider + "physO.velocity.y: " + physO.velocity.y + " velocityCap.y: " + velocityCap.y) ;
        if (objectCollider.IsTouching(anotherCollider) && motionTest.magnitude > 0.1f)
        {
            
            foreach (ContactPoint2D hit in collision.contacts)
            {
                hitPosition.x = hit.point.x - 0.01f * hit.normal.x;
                hitPosition.y = hit.point.y - 0.01f * hit.normal.y;
                tilemap.SetTile(tilemap.WorldToCell(hitPosition), null);
            }
            pPos = tilemap.WorldToCell(collision.otherCollider.transform.position);
            TileBase tBase = tilemap.GetTile(new Vector3Int(0, 0, 0));
            tilemap.SetTile(new Vector3Int(0, 0, 0), null);
        }
    }
    private void OnMouseDrag()
    {
        if (physO.gravityMod == 1f)
        {
            physO.gravityMod = 0f;
            //patrol.gravityMod = 0f;
        }
        

        Vector2 worldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        Vector2 moveAmount = worldPos - new Vector2(transform.position.x, transform.position.y);

        physO.Movement(moveAmount.y * Vector2.up, true);

        physO.Movement(moveAmount.x * Vector2.right, false);
    }


    // Update is called once per frame
    void Update()
    {

    }
    private void OnMouseUp()
    {
        physO.gravityMod = 1f;
        //patrol.gravityMod = 0f;
    }
}
