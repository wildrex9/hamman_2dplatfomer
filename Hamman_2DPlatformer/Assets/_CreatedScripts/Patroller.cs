﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class Patroller : PhysicsObject
{

    public float maxSpeed = 1f;
    bool goRight;


    protected override void ComputeVelocity()
    {
            Vector2 move = Vector2.right;
            if (!goRight)
                move = Vector2.left;

            targetVelocity = move * maxSpeed;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
            if (collision.gameObject.CompareTag("PatrolMarker"))
            {
                goRight = !goRight;
            }
    }
}
